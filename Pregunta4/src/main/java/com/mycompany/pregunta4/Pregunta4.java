/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.pregunta4;

import java.util.Scanner;

/**
 *
 * @author Luis Sanchez <luis.sanchez.d@uni.pe>
 */
public class Pregunta4 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Ingrese 7 numeros: ");
        double sum = 0;
        double[] numeros = new double[100];

        for (int i = 0; i < 7; i++) {
            double num = input.nextDouble();
            numeros[i] = num;
            sum = sum + num;
        }

        System.out.print("Notas: ");
        for (int i = 0; i < 7; i++) {
            System.out.print(numeros[i] + " ");
        }
        double prom = sum/7;
        System.out.println("\nPromedio: " + prom);
    }
}
